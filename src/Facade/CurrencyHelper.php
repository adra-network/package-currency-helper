<?php

namespace Adranetwork\CurrencyHelper\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Adranetwork\CurrencyHelper\CurrencyHelper
 */
class CurrencyHelper extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'currency-helper';
    }
}
