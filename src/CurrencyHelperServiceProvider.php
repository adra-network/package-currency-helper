<?php

namespace Adranetwork\CurrencyHelper;

use Illuminate\Support\ServiceProvider;

class CurrencyHelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Register the main class to use with the facade
        $this->app->singleton('currency-helper', function () {
            return new CurrencyHelper;
        });
    }
}
