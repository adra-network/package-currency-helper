<?php

namespace Adranetwork\CurrencyHelper\Tests;

use Adranetwork\CurrencyHelper\CurrencyHelperServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{

    protected function getPackageProviders($app): array
    {
        return [
            CurrencyHelperServiceProvider::class,
        ];
    }
}
