<?php

namespace  Adranetwork\CurrencyHelper\Tests;

use Adranetwork\CurrencyHelper\Facade\CurrencyHelper;
use Illuminate\Foundation\Testing\WithFaker;
use PrinsFrank\Standards\Currency\ISO4217_Alpha_3;

class CurrencyHelperTest extends TestCase
{

    use WithFaker;

    /** @test **/
    public function it_can_determine_if_non_decimal_currency ()
    {
         $this->assertTrue(CurrencyHelper::isNonDecimal('JPY'));
         $this->assertFalse(CurrencyHelper::isNonDecimal(ISO4217_Alpha_3::US_Dollar));
    }

    /** @test **/
    public function it_can_set_currency_via_alpha_3_name ()
    {
        $expected = ISO4217_Alpha_3::US_Dollar;
        $this->assertEquals($expected, CurrencyHelper::setCurrency('USD')->getCurrency());
    }

    /** @test **/
    public function it_can_set_currency_via_iso_4217()
    {
        $expected = ISO4217_Alpha_3::US_Dollar;
        $this->assertEquals($expected, CurrencyHelper::setCurrency($expected)->getCurrency());
    }
    /** @test **/
    public function provided_currency_must_be_valid ()
    {
         $this->expectException(\Exception::class);
         CurrencyHelper::setCurrency('e');
    }

    /** @test **/
    public function it_can_set_currency_and_amount_via_of_method_using_alpha_3 ()
    {
        $amount = 5000;
        $currency = 'EUR';
        $helper = CurrencyHelper::of($currency, $amount);
        $this->assertEquals($amount, $helper->getAmount());
        $this->assertEquals(ISO4217_Alpha_3::Euro, $helper->getCurrency());
    }
    /** @test **/
    public function it_can_set_currency_and_amount_via_of_method_using_iso_4217 ()
    {
        $amount = 5000;
        $currency = ISO4217_Alpha_3::Euro;
        $helper = CurrencyHelper::of($currency, $amount);
        $this->assertEquals($amount, $helper->getAmount());
        $this->assertEquals($currency, $helper->getCurrency());
    }

    /** @test **/
    public function it_can_get_amount ()
    {
        $this->assertEquals(5000, CurrencyHelper::setAmount(5000)->getAmount());
    }
    /** @test **/
    public function it_supports_lower_cased_alpha_3_currency ()
    {
        $this->assertEquals(
            ISO4217_Alpha_3::Euro,
            CurrencyHelper::setCurrency('eur')->getCurrency());
    }

    /** @test **/
    public function it_can_get_currency()
    {
        $this->assertEquals(
            ISO4217_Alpha_3::Euro,
            CurrencyHelper::setCurrency(ISO4217_Alpha_3::Euro)->getCurrency());
    }

    /** @test **/
    public function it_can_convert_to_human_readable_amount_decimal_currency ()
    {
        $rawAmount = 654; // represent $5
        $currency = 'USD'; // represent $5


        $this->assertEquals(6.54,
            CurrencyHelper::getHumanReadableAmount($currency, $rawAmount));
    }


    /** @test **/
    public function it_can_convert_to_human_readable_amount_non_decimal_currency ()
    {
        $rawAmount = '500.00'; // represent $5
        $currency = ISO4217_Alpha_3::Yen;

        $this->assertEquals($rawAmount,
            CurrencyHelper::getHumanReadableAmount($currency, $rawAmount));
    }


    /** @test **/
    public function it_can_return_a_list_of_non_decimal_currencies ()
    {
         $this->assertEquals(15, count(CurrencyHelper::getNonDecimalCurrencies()));
    }

}
