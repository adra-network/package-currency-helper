# Currency Helper

[![Latest Version on Packagist](https://img.shields.io/packagist/v/adranetwork/currency-helper.svg?style=flat-square)](https://packagist.org/packages/adranetwork/currency-helper)
[![Total Downloads](https://img.shields.io/packagist/dt/adranetwork/currency-helper.svg?style=flat-square)](https://packagist.org/packages/adranetwork/currency-helper)

Helps parsing amount depending of the currency type (decimal, non-decimals)


## Installation

You can install the package via composer:

```bash
composer require adranetwork/currency-helper
```

## Examples
Usable methods can be found in `Adranetwork\CurrencyHelper\CurrencyHelper.php`
```php
CurrencyHelper::isNonDecimal(String | ISO4217_Alpha_3) 

CurrencyHelper::setCurrency(String | ISO4217_Alpha_3)
            ->setAmount(Int)
            ->getHumanReadableAmount() 
            
CurrencyHelper::of(String | ISO4217_Alpha_3, Int)->getHumanReadableAmount()

```

### Testing

```bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email info@auclair.com instead of using the issue tracker.

## Credits

-   [Francois Auclair](https://github.com/adranetwork)
-   [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).
